"""
PostIt.py
A simple note maker

Author: Richard Harrington
Date Created: 9/8/2013
"""

from tkinter import *
from datetime import datetime
class PostIt():
    root=Tk()
    text= Text(root)

    def create(self):
        msgtext=self.text.get(0.0,END)
        note=Toplevel()
        m=Message(note,text=msgtext)
        m.pack()
        note.title(datetime.now())

    
    def __init__(self,parent=None, file=None):
        # Put everything together
        b=Button(self.root,text="Create post-it",command=self.create)
        b.pack(side=BOTTOM)
        
        self.text.pack(side=TOP)
        
        self.root.title("Post-Its")
        self.root.mainloop()


pi=PostIt()
